[[`🏠на главную страницу`]](/README.md)
# Обучалка по использованию GIT

- [Обучалка по использованию GIT](#обучалка-по-использованию-git)
	- [Не стоит путать git и github](#не-стоит-путать-git-и-github)
	- [Идея, на которой основан git](#идея-на-которой-основан-git)
	- [Что нужно для работы с git](#что-нужно-для-работы-с-git)
	- [Состояния файлов в GIT](#состояния-файлов-в-git)
	- [Основные команды GIT](#основные-команды-git)
	- [Разбор использования git в командной строке](#разбор-использования-git-в-командной-строке)
	- [Конфликты слияния](#конфликты-слияния)
	- [Git Blame](#git-blame)
	- [Переименование файлов в git](#переименование-файлов-в-git)
	- [.gitignore](#gitignore)
	- [Текстовый редактор для git по-умолчанию](#текстовый-редактор-для-git-по-умолчанию)

---

Я решил написать об этом здесь, т.к. рано или поздно эта информация понадобится.  
GIT или аналоги с теми же принципами нужны будут практически в любом проекте, связанном с разработкой.  
И чем раньше начать его использовать - тем удобнее.  

По git есть сотни очень хороших гайдов и мануалов в любом формате, поэтому здесь я расскажу основное, но далеко не всё.  
Пользоваться git достаточно просто, не нужно бояться его освоить.

## Не стоит путать git и github

**git** - программа, система управления версиями, чаще всего используемая в программировании  
**github** - сайт, на котором публикуются git-репозитории (хранилища проектов git).  
Одним из аналогов **github** является **gitlab**, лично мне кажущийся более удобным.  
И github, и gitlab работают с программой git.

## Идея, на которой основан git

Git - это система контроля версий для разработки.

**Проблема**: ты делаешь одну программу и меняешь её много раз, нужно хранить много её версий.

![мем](/img/git/github_meme.png)

А представь, сколько версий требуют крупные проекты, с гигабайтами кода и сотнями одновременно разрабатываемых разными людьми функций, тестов, исправлений.

**Решение**: вместо того, чтобы хранить полные наборы файлов отдельно, git представляет проект в виде списка изменений.

Каждое "сохранение" изменений называется ***коммит (commit)***.

При создании коммита, git смотрит, что изменилось по сравнению с предыдущим коммитом, и сохраняет лишь эти изменения:

![1](/img/git/1.png)  
![2](/img/git/2.png)  

\- вот, как могут храниться изменения.  
Вместо копий файла с тысячами строк, мы храним лишь те строки, которые изменились.

В графическом редакторе те же изменения обычно выглядят удобнее и нагляднее:  

![3](/img/git/3.png)  
![4](/img/git/4.png)  

Коммиты следуют друг за другом, содержат изменения, которые "наслаиваются" друг на друга - получаются цепочки изменений:  

![5](/img/git/5.png)  

У каждого коммита есть хэш (уникальный 16-ричный номер в формате `9b655f46ccd5c0920f3befe6a036f3a96bee78b6`), автор, время и т.д.  

В любой момент можно переключиться на любой из коммитов или откатить изменения. Это полезно, если в программе возник баг, и нужно найти из-за чего и когда он появился: идти по списку коммитов, тестировать его наличие в каждом из них

Также история коммитов не линейна - в git есть ветки.  
Вместо того, чтобы все коммиты шли последовательно, дерево может разделяться:

![6](/img/git/6.png)  

От любого коммита можно ответвиться, начать свою историю изменений.  
Так, два разработчика могут работать над разными функциями одновременно, не ломая друг другу код.  
Ветки также можно "вливать" друг в друга (называется мердж - merge)

Обычно есть ветка со стабильной версией проекта (на которой всё работает), от неё ответвляются, чтобы сделать что-то, и в неё вливают, после того, как функционал закончен.

*Вот пример, где что-то похожее происходило:*

![7](/img/git/7.png)  

Когда ветка вливается, изменения из этих веток совмещаются, но если есть конфликты (два разных изменения одного и того же), то они разрешаются вручную (подробнее [ниже](#конфликты-слияния)).

## Что нужно для работы с git

`git` изначально является командой-утилитой для консоли  
Для него также есть возможности работать с git-репозиториями и с графическим интерфейсом.  
Лично мне больше всего нравится среда VS Code с дополнениями Git Lens и Git Graph 

Для работы с удалённым репозиторием, нужно иметь к нему доступ.
Получение доступа по http/https ссылке происходит с вводом логина/пароля от сервиса.

Чтобы не вводить логин и пароль каждый раз, используются SSH-ключи и SSH-ссылки на репозиторий.
Документация GitLab по SSH-ключам (для GitHub - аналогично): https://docs.gitlab.com/ee/user/ssh.html

Самый базовый вариант:

1. ввести команду ssh-keygen -t ed25519
2. оставить запрашиваемые поля пустыми
3. в папке ~/.ssh появятся файлы id_ed25519 и id_ed25519.pub, содержащие приватный и публичный ключи соответственно
4. скопировать публичный ключ (содержимое файла id_ed25519.pub)
5. добавить его на странице настроек GitHub, GitLab или другого сервиса

Или можно сделать настройку командой `git config credential.helper store`, чтобы логин и пароль сохранялись


---
## Состояния файлов в GIT

Файлы могут быть в четырёх состояниях относительно git:

- **Unmodified** - файл без изменений.  
  Это состояние файла в проекте по-умолчанию, т.е. состояние, в котором он полностью совпадает со своим состоянием на HEAD (текущем коммите)  
  После создания нового коммита все файлы из статуса **Staged** переходят в статус **Unmodified**  

- **Modified** - есть изменения (не связанные с git).  
  Этот файл существует в проекте, но отличается от своей версии с текущего коммита.  

- **Staged** - индексированные изменения.  
  Git позволяет выбирать, какие из изменений будут добавлены в коммит.  
  Индексированные файлы - это те файлы, изменения из которых были выбраны для добавления.  

  > Индексируется именно состояние файла, а не сам файл.  
  > Можно изменить файл, добавить в индекс и снова изменить - тогда файл с первыми изменениями будет иметь статус staged, а со вторыми - modified.  
  > Если после этого добавить изменения ещё раз, то в индексе (staged) будет комбинация всех изменений.

- **Untracked** - не отслеживаемый файл.  
  Этот файл - новый, его не существовало в текущем проекте до этого момента и изменения в нём не отслеживаются.  
  Как и изменённые файлы, его можно добавить в индекс (и он войдёт в коммит точно так же, как и изменения существующих файлов)  
  При этом такие файлы, в отличие от файлов проекта, могут быть удалены командой `git clean`

  ![Состояния файлов](/img/git/file_states.png)

## Основные команды GIT

Ниже перечислены основные команды git, которые могут пригодиться при работе, и краткое описание то, зачем они могут быть нужны.

Если ещё не понятно, что в этих командах происходит, рекомендую сначала прочитать разбор примеров использования [в разделе ниже](#разбор-использования-git-в-командной-строке)

Подробнее об использовании команд можно прочитать в документации: https://git-scm.com/docs  
Также информация о командах git есть в системной документации linux (команда `man`)  
К примеру, документацию по команде `git pull` можно открыть командой `man git-pull`

**Список базовых команд:**

- `git fetch` - получение всех изменений / обновлений из удалённого репозитория
  - с параметром `--prune` : удаление данных о несуществующих удалённых ветках

- `git status` - вывод состояния проекта, в том числе текущую ветку / коммит, список изменённых файлов и т.д.

- `git checkout` - переключение между ветками, тэгами и коммитами  
  - в определённых случаях - создание веток (опции `-b`, `--orphan` и др.)

- `git log` - просмотр истории предшествующих коммитов

- `git diff`
  - просмотр текущих изменений
  - просмотр изменений по сравнению с индексом (`-–staged`)
  - просмотр различий между любыми ветками, коммитами, тэгами и т.д.
  - создание патчей (если вывод перенаправить в файл);  
    патчи применяются командой `git apply`

- `git add` - индексирование изменений (для добавления в коммит)  
  Примеры:
  - `git add *` добавляет все добавленные и изменённые файлы
  - `git add src/*.c` добавляет все файлы с расширением .c из папки src и её подпапок
  - `git add :*` добавляет все удалённые файлы

- `git commit`
  - Создание коммита
  - Редактирование коммита в соответствии с индексом (опция `--amend`)

- `git branch` - Управление ветками
  - Просмотр списка веток (без опций)
  - Создание веток (опция = название)
  - Переименование веток (`-m`)
  - Удаление веток (`-D`)

- `git tag` - Управление тэгами  
  (тэг - именованный указатель на коммит, по сути - неизменяемая ветка)  
  - Создание, удаление, переименование тегов и т.д.

- `git pull`
  - выполняет `git fetch` + обновляет текущую ветку в соответствии с удалённой, если удалённая новее

- `git push` - загружает ветку или тэг в удалённый репозиторий
  - при добавлении : перед названием ветки/тега, удаляет их из удалённого репозитория

  - команда `git push` (без `:`) не перезаписывает историю коммитов и не допускает потери данных, если не указана опция `--force`  
  - (!) опцию `--force` лучше не использовать совсем; она допускается только в исключительных случаях и с большой аккуратностью!  

    > Для безопасной отмены изменений и аналогичных случаев есть другие команды такие как git revert (создаёт новый отменяющий коммит, сохраняя историю)

- `git merge` - вливание одной ветки в другую

- `git cherry-pick` - создание копии выбранного коммита (или нескольких) на текущей ветке

- `git reset`
  - сброс добавленных в индекс файлов (без опций)
  - сброс всех текущих изменений (`--hard`)
  - сброс текущей ветки до состояние любого коммита (использовать аккуратно!)
  - наличие или отсутствие опций `--soft` / `--hard` определяет, изменяются ли файлы / индекс

- `git rebase`
  - перестроение текущей ветки на основе другой
  - редактирование истории коммитов (режим `--interactive`, использовать аккуратно!)

- `git revert`
  - Отменяет изменения коммита (создаёт новый коммит, убирающий изменения)

- `git apply`
  - Применяет патчи (созданные, например, коммандой `git diff`)

- `git show`
  - Просмотреть коммит

- `git init`
  - Инициализация нового git-репозитория (локально)

- `git clone`
  - Клонирование (получение) нового git-репозитория из другого (чаще всего удалённого)

- `git remote`
  - Добавление, удаление и редактирование удалённых репозиториев (remote)

- `git clean` - очистка неотслеживаемых git файлов (аккуратно!)

- `git stash`
  - сохранить неиндексированные изменения и временно их убрать / "спрятать"
  - вернуть можно командой `git stash apply` (оставляет stash) или `git stash pop` (сбрасывает)

- `git config` - настройка и просмотр параметров git
  - локальных (по-умолчанию)
  - глобальных (с опцией `--global`)

---
## Разбор использования git в командной строке

```bash
~ > mkdir example 
~ > cd example/
~/example > git init # Создаём репозиторий git
```
```bash
...
Initialized empty Git repository in /home/artiom/example/.git/
```
```bash
# Мы должны сказать гиту, от чьего имени делать коммиты. 
~/example > git config user.name "Vasiliy Pupkin"       
~/example > git config user.email vaspupkin@muhosran.sk 

# Если в git config указать опцию --global, то настройки будут применяться для компьютера, а не только для текущего проекта

~/example > echo "ABCDF" >file1 # Создаём какой-то файл
~/example > echo "ABDC" >file2 # Создаём какой-то ещё

~/example > git status
# Эта команда позволяет проверить состояние, в том числе какие файлы изменены и какие проиндексированы
```
```bash
On branch master

No commits yet

Untracked files:
(use "git add <file>..." to include in what will be committed)
file1
file2

nothing added to commit but untracked files present (use "git add" to track)
```
```bash
~/example > git add file1
# Команда индексирует изменения в файле file1, но ещё не создаёт коммит

~/example > git status
```
```bash
On branch master

No commits yet

Changes to be committed:
(use "git rm --cached <file>..." to unstage)
new file: file1

Untracked files:
(use "git add <file>..." to include in what will be committed)
file2
```
```bash
~/example > git commit -m "Added file 1" 
# Создаём коммит. Если не указать опцию -m <название>, то откроется текстовый редактор для редактирования сообщения коммита.
```
```bash
[master (root-commit) 6d165b5] Added file 1
1 file changed, 1 insertion(+)
create mode 100644 file1
```
```bash
~/example > git status
```
```bash
On branch master
Untracked files:
(use "git add <file>..." to include in what will be committed)
file2

nothing added to commit but untracked files present (use "git add" to track)

# Из текущих несохранённых изменений в проекте остался только file2
```
```bash
~/example > git log
```
```bash
commit 6d165b5cc558388c6828b52994d22be2bf9802c4 (HEAD -> master)
Author: Vasiliy Pupkin <vaspupkin@muhosran.sk>
Date: Thu Feb 18 13:40:27 2021 +0300

Added file 1

# Эта команда показывает историю коммитов. Сейчас на ветке master - один коммит
```
```bash
~/example > echo "O_o" >>file1

# Добавили строчку в file1 

~/example > git status
```

```bash
On branch master
Changes not staged for commit:
(use "git add <file>..." to update what will be committed)
(use "git restore <file>..." to discard changes in working directory)
modified: file1

Untracked files:
(use "git add <file>..." to include in what will be committed)
file2

no changes added to commit (use "git add" and/or "git commit -a")
```

```bash
# Теперь в проекте один файл - untracked (не отслеживаемый, т.к. его ещё не было в проекте), и один - modified (изменённый)

~/example > git diff # Посмотрим изменения в репозитории.
```
```diff
diff --git a/file1 b/file1
index 94b4c5f..86e2226 100644
--- a/file1
+++ b/file1
@@ -1 +1,2 @@
ABCDF
+O_o
```
```bash
# Как видим выше, изменён только один файл
# Вообще говоря, такой формат изменений выводится для каждого


~/example > git add file1 # Индексируем
~/example > git status
```
```bash
On branch master
Changes to be committed:
(use "git restore --staged <file>..." to unstage)
modified: file1
# Изменения этого файла проиндексированы

Untracked files:
(use "git add <file>..." to include in what will be committed)
file2
# Этот до сих пор не отслеживаемый, что бы мы в нём не изменили, он бы всё равно остался untracked
```
```bash
~/example > git add file2 # Добавляем file2
~/example > git status
```
```bash
On branch master
Changes to be committed:
(use "git restore --staged <file>..." to unstage)
modified: file1
new file: file2
```
```bash
# Коммит будет из двух изменений: добавленный и изменённый файлы

~/example > git diff

# Команда выводит ничего, т.к. нет изменений в сравнении с индексом

~/example > git diff --staged
# С опцией --staged мы сравниваем последний коммит с индексом
```
```diff
diff --git a/file1 b/file1
index 94b4c5f..86e2226 100644
--- a/file1
+++ b/file1
@@ -1 +1,2 @@
ABCDF
+O_o
diff --git a/file2 b/file2
new file mode 100644
index 0000000..0680cbd
--- /dev/null
+++ b/file2
@@ -0,0 +1 @@
+ABDC
```
```bash
~/example > git commit -m "file2 and some changes in file 1, wow, I am doing great" # Делаем новый коммит
```
```bash
[master f9f1fc7] file2 and some changes in file 1, wow, I am doing great
2 files changed, 2 insertions(+)
create mode 100644 file2
```
```bash
~/example > git status
```
```bash
On branch master # Кстати, мы на ветке master
nothing to commit, working tree clean

# Изменений больше нет
```
```bash
~/example > git log
```
```bash
commit f9f1fc7a8c5c1e95ee7097ca2a5b3058bea01ac2 (HEAD -> master)
Author: Vasiliy Pupkin <vaspupkin@muhosran.sk>
Date: Thu Feb 18 13:45:00 2021 +0300

file2 and some changes in file 1, wow, I am doing great

commit 6d165b5cc558388c6828b52994d22be2bf9802c4
Author: Vasiliy Pupkin <vaspupkin@muhosran.sk>
Date: Thu Feb 18 13:40:27 2021 +0300

Added file 1

# В логе два коммита
```
```bash
~/example > git remote add origin ../example.git/ 

# Добавляем удалённый репозиторий
# origin - это название, может быть любым
# ../example.git - путь к репозиторию, в данном случае он находится на локальном компьютере 
# (создан командой git init --bare ../example.git, опция --bare делает его пустым, в который можно делать push и pull)

# Удалённые репозитории добавляются точно так же, только вместо этого пути начинаются с http://, https://, или ssh://
# Команда git clone <адрес> сразу скачивает удалённый репозиторий и настраивает в нём remote

~/example > git push origin master
# Так мы отправляем ветку master в удалённый репозиторий
```
```bash
Enumerating objects: 7, done.
Counting objects: 100% (7/7), done.
Delta compression using up to 8 threads
Compressing objects: 100% (3/3), done.
Writing objects: 100% (7/7), 516 bytes | 516.00 KiB/s, done.
Total 7 (delta 0), reused 0 (delta 0), pack-reused 0
To ../example.git/
* [new branch] master -> master
```
```bash
~/example > git pull origin master
# Получаем ветку master из удалённого репозитория
```
```bash
From ../example
* branch master -> FETCH_HEAD
Already up to date.
# Там не было ничего, чего нет в нашем проекте

```
```bash
~/example > git checkout -b newbranch
# Создаём новую ветку (также можно командой git branch)
```
```bash
Switched to a new branch 'newbranch' 
```
```bash
~/example > echo "ABCD" >file2 # Перезаписываем файл

~/example > git status
```
```bash
On branch newbranch # Заметим, что сейчас мы на ветке newbranch
Changes not staged for commit:
(use "git add <file>..." to update what will be committed)
(use "git restore <file>..." to discard changes in working directory)
modified: file2 # Файл изменён

no changes added to commit (use "git add" and/or "git commit -a")
```
```bash
~/example > git diff # Смотрим изменения файла
```
```diff
diff --git a/file2 b/file2
index 0680cbd..9ed4614 100644
--- a/file2
+++ b/file2
@@ -1 +1 @@
-ABDC
+ABCD
```
```bash
~/example > git add file2 # Индексируем

~/example > git commit -m "Fixed letter order" # Делаем коммит
```
```bash
[newbranch 359da49] Fixed letter order # Он уже на newbranch, а не на master
1 file changed, 1 insertion(+), 1 deletion(-)

~/example > git status
```
```bash
On branch newbranch # Всё ещё тут
nothing to commit, working tree clean
```
```bash
~/example > git log 
```
```bash
# HEAD - то, где мы сейчас:
commit 359da49aa515b702c355051b0e43cf5260ae703f (HEAD -> newbranch)
Author: Vasiliy Pupkin <vaspupkin@muhosran.sk>
Date: Thu Feb 18 14:06:01 2021 +0300

Fixed letter order


# ветка master, и её версия из удалённого репозитория origin:
commit 98e101427e58c8907f429715dc3ee62c88cb90dc (origin/master, master) 
Author: Vasiliy Pupkin <vaspupkin@muhosran.sk>
Date: Thu Feb 18 14:01:43 2021 +0300

file2 and some changes in file 1, wow, I am doing great

commit 6d165b5cc558388c6828b52994d22be2bf9802c4
Author: Vasiliy Pupkin <vaspupkin@muhosran.sk>
Date: Thu Feb 18 13:40:27 2021 +0300

Added file 1

```
```bash
~/example > git checkout master # Вернулись
```
```bash
Switched to branch 'master'
```
```bash
~/example > git status
# Проверили, что вернулись (не обязательно)
```
```bash
On branch master
nothing to commit, working tree clean
```
```bash
~/example > git log
# Смотрим лог, последний коммит - тот, который делали на master
```
```bash
# Смотрите, HEAD переместилась:
commit 98e101427e58c8907f429715dc3ee62c88cb90dc (HEAD -> master, origin/master) 
Author: Vasiliy Pupkin <vaspupkin@muhosran.sk>
Date: Thu Feb 18 14:01:43 2021 +0300

file2 and some changes in file 1, wow, I am doing great

commit 6d165b5cc558388c6828b52994d22be2bf9802c4
Author: Vasiliy Pupkin <vaspupkin@muhosran.sk>
Date: Thu Feb 18 13:40:27 2021 +0300

Added file 1
```
```bash
~/example > git checkout newbranch # Снова тут
```
```bash
Switched to branch 'newbranch'
```
```bash
# Я просто повторяю git status, чтобы было нагляднее
~/example > git status
```
```bash
On branch newbranch
nothing to commit, working tree clean
```
```bash
# Снова видим то, что было раньше
~/example > git log
```
```bash
commit 359da49aa515b702c355051b0e43cf5260ae703f (HEAD -> newbranch)
Author: Vasiliy Pupkin <vaspupkin@muhosran.sk>
Date: Thu Feb 18 14:06:01 2021 +0300

Fixed letter order

commit 98e101427e58c8907f429715dc3ee62c88cb90dc (origin/master, master)
Author: Vasiliy Pupkin <vaspupkin@muhosran.sk>
Date: Thu Feb 18 14:01:43 2021 +0300

file2 and some changes in file 1, wow, I am doing great

commit 6d165b5cc558388c6828b52994d22be2bf9802c4
Author: Vasiliy Pupkin <vaspupkin@muhosran.sk>
Date: Thu Feb 18 13:40:27 2021 +0300

Added file 1

```
```bash
# Отправляем ветку newbranch в репозиторий origin
~/example > git push origin newbranch
```
```bash
Enumerating objects: 5, done.
Counting objects: 100% (5/5), done.
Delta compression using up to 8 threads
Compressing objects: 100% (2/2), done.
Writing objects: 100% (3/3), 279 bytes | 279.00 KiB/s, done.
Total 3 (delta 0), reused 0 (delta 0), pack-reused 0
To ../example.git/
* [new branch] newbranch -> newbranch
```
```bash
# Вернулись на master
~/example > git checkout master
```
```bash
Switched to branch 'master'
```
```bash
# Вливаем ветку newbranch в master
~/example > git merge newbranch
```
```bash
Updating 98e1014..359da49
Fast-forward
file2 | 2 +-
1 file changed, 1 insertion(+), 1 deletion(-)
```
```bash
# Недобавленных изменений нет
~/example > git status
```
```bash
On branch master
nothing to commit, working tree clean
```
```bash
# Но теперь ветка master совпадает с newbranch
~/example > git log
```
```bash
commit 359da49aa515b702c355051b0e43cf5260ae703f (HEAD -> master, origin/newbranch, newbranch) 
Author: Vasiliy Pupkin <vaspupkin@muhosran.sk>
Date: Thu Feb 18 14:06:01 2021 +0300

Fixed letter order

# А origin до сих пор думает, что master здесь:
commit 98e101427e58c8907f429715dc3ee62c88cb90dc (origin/master)
Author: Vasiliy Pupkin <vaspupkin@muhosran.sk>
Date: Thu Feb 18 14:01:43 2021 +0300

file2 and some changes in file 1, wow, I am doing great

commit 6d165b5cc558388c6828b52994d22be2bf9802c4
Author: Vasiliy Pupkin <vaspupkin@muhosran.sk>
Date: Thu Feb 18 13:40:27 2021 +0300

Added file 1
```
```bash
~/example > git push origin master # Отправляем удалённую master
```
```bash
Total 0 (delta 0), reused 0 (delta 0), pack-reused 0
To ../example.git/
98e1014..359da49 master -> master
```
```bash
~/example > git log
```
```bash
commit 359da49aa515b702c355051b0e43cf5260ae703f (HEAD -> master, origin/newbranch, origin/master, newbranch) # Теперь все здесь
Author: Vasiliy Pupkin <vaspupkin@muhosran.sk>
Date: Thu Feb 18 14:06:01 2021 +0300

Fixed letter order

commit 98e101427e58c8907f429715dc3ee62c88cb90dc
Author: Vasiliy Pupkin <vaspupkin@muhosran.sk>
Date: Thu Feb 18 14:01:43 2021 +0300

file2 and some changes in file 1, wow, I am doing great

commit 6d165b5cc558388c6828b52994d22be2bf9802c4
Author: Vasiliy Pupkin <vaspupkin@muhosran.sk>
Date: Thu Feb 18 13:40:27 2021 +0300

Added file 1
```

Вроде бы я продемонстрировал основные возможности.  
Конечно же, это не все - в git есть множество команд для разных целей, простых или сложных.

---

## Конфликты слияния

Если вдруг в процессе применения патча / коммита, слияния веток или rebase изменения не применяются однозначно, git сообщит о конфликте.  
Чаще всего конфликты происходят, когда одна и та же часть кода изменена по-разному.  

В таких случаях, в коде вместо результата слияния возникнет подобная конструкция:

```
 <<<<<<< ...
 версия 1
 =======
 версия 2
 >>>>>>> ...
```

Для разрешения конфликта нужно вручную разрешить конфликт (оставить ту версию, которая должна быть), затем добавить эти изменения командой git add и продолжить процесс  
`(git commit / git merge --continue / git rebase --continue / git cherry-pick --continue / ...)`

Также действия все команды, которые могут вызвать конфликт слияния, можно отменить опцией `--abort`; файлы вернутся в состояние до этой команды  
(e.g. `git merge --abort`)


## Git Blame

С помощью команды git blame и аналогичных функций графической среды можно просмотреть, в каких коммитах изменялась конкретная строчка кода.  
К примеру, если конкретный отрезок кода вызывает вопросы и не понятно, почему так было сделано, можно узнать, в каком последнем коммите в неё внесли изменения.  
По коммиту можно понять, кто эти изменения внёс и в рамках какой задачи они произошли, затем найти эту задачу или даже этого человека.  

## Переименование файлов в git

Изначально git воспринимает переименование файла как "удаление старого + создание нового"  
Однако, при индексации, если есть новый файл, в большей части совпадающий со старым, git может отметить его как "renamed".  
Этот вариант предпочтительнее, т.к. для переименованного файла сохраняется история.  
Поэтому, если есть необходимость переименовать файл и сделать в нём изменения, на стадии разработки возможно лучше сделать отдельный коммит с переименованием.  

## .gitignore

Часто в директориях git проекта можно встретить файл .gitignore  
В нём задаются файлы, изменения которых будут игнорироваться git  
К примеру, строка *.o в этом файле говорит о необходимости игнорировать все файлы с расширением .o (т.е. объектные файлы)  
Игнорируемые файлы в том числе не показываются в выводе команды git status  

## Текстовый редактор для git по-умолчанию

Следующие команды помогают выбрать редактор по умолчанию:  
`git config core.editor vim`  
`git config core.editor nano`  
`git config core.editor "code --wait"`  

(для vs code нужна опция --wait, т.к. команда git commit ожидает закрытия редактора перед коммитом)

